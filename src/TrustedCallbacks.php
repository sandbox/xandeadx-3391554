<?php

namespace Drupal\recaptcha_v3_pagespeed;

use Drupal\Core\Security\TrustedCallbackInterface;

class TrustedCallbacks implements TrustedCallbackInterface {

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks(): array {
    return ['htmlTagPreRender'];
  }

  /**
   * #pre_render callback for "html_tag" element.
   */
  public static function htmlTagPreRender(array $element): array {
    if ($element['#tag'] == 'script' && !empty($element['#attributes']['timeout'])) {
      $element['#value'] = <<<JS
        setTimeout(function () {
          const script = document.createElement('script');
          script.src = '{$element['#attributes']['src']}';
          script.async = true;
          document.getElementsByTagName('head')[0].appendChild(script);
        }, {$element['#attributes']['timeout']});
      JS;
      unset($element['#attributes']);
    }

    return $element;
  }

}
